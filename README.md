# Epson Projector Controller

Provides control of Epson Projecotors.

## General Philosophy 
This controller assumes it is the only one controlling the projector. It will try to get the projector to be in the same state as the model. If it finds a projector state out of sync with the model it will try to change the projector state to match the model. With the current features there is not reason to send the current "controllable state" back to the user of this controller. We just keep trying to set the projector to the user's desired state. This vastly reduces the chances race condistions between the user state and the projector state. 


## Current State of Implementation
#### General
✅ Connection establishment, maintenance
✅ Polling
✅ Responce parcing
❌ Multiple command q'ing

#### Controllable State
✅ Power
✅ Mute
✅ Source (HDMI & HDBaseT only)

#### Status State
✅ Controller Connection
✅ Lamp Hours
❌ Serial Number 
❌ Model Name

## Epson Docs
[Here is where you should be able to get the latest version.](https://epson.com/Support/wa00740)
Note that the version consulted at time of coding is saved in this repo in case Epson takes down the documents.
