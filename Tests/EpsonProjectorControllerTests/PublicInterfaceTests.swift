import XCTest
import Network
import Combine
@testable import EpsonProjectorController

final class PublicInterfaceTests: XCTestCase {
    let q = DispatchQueue(label: "MocProjector")
    let mocProjector = try? NWListener(using: .tcp, on: 3629)
    func handleHandShake(connection:NWConnection){
        connection.start(queue: self.q)
        connection.receive(minimumIncompleteLength: 16, maximumLength: 16){data, _, _, _ in
            guard let data = data else { return }
            if data == handshakeInit {
                connection.send(content: handshakeResponce, completion: .contentProcessed({_ in}))
            }
            self.nextHandler?(connection)
        }
    }
    var nextHandler:((NWConnection) -> Void)?
    
    func test1PowerOn(){
        mocProjector?.parameters.allowLocalEndpointReuse = true //This is needed to be able to run the tests fast. 
        //Testing WD catches connection issues after Handshake
        let powerOnRecieved = XCTestExpectation(description: "Handshake Successful")
        powerOnRecieved.expectedFulfillmentCount = 1
        
        mocProjector?.newConnectionHandler = handleHandShake
        self.mocProjector?.start(queue: self.q)
        
        let projector = EpsonProjector()

        let sub = projector.$connectionStatus.sink{ status in
            if status == .Connected {
                self.q.asyncAfter(deadline: .now() + 1) {
                    // Async is to not hit the Q
                    projector.power = true
                }
            }
        }
        mocProjector?.newConnectionHandler = handleHandShake
        nextHandler = { connection in
            connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                guard let data = data else { return }
                if String(bytes: data, encoding: .ascii) ==  "PWR ON\r\n"{
                    powerOnRecieved.fulfill()
                }
            }
        }
        projector.host = "localhost"

        wait(for: [powerOnRecieved], timeout: 2)
        mocProjector?.cancel()
        sub.cancel()
    }
    
    func test2PowerAsserted(){
        mocProjector?.parameters.allowLocalEndpointReuse = true
        //Testing WD catches connection issues after Handshake
        let powerAsserted = XCTestExpectation(description: "Power State Asserted")
        powerAsserted.expectedFulfillmentCount = 1
        
        mocProjector?.newConnectionHandler = handleHandShake
        self.mocProjector?.start(queue: self.q)
        
        let projector = EpsonProjector()
        projector.power = true

        mocProjector?.newConnectionHandler = handleHandShake
        nextHandler = { connection in
            connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                guard let data = data else { return }
                if String(bytes: data, encoding: .ascii) ==  "PWR ON\r\n"{
                    connection.send(content: "PWR=04\r:".data(using: .ascii), completion: .contentProcessed({_ in}))
                    connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                        guard let data = data else { return }
                        if String(bytes: data, encoding: .ascii) ==  "PWR ON\r\n"{
                            powerAsserted.fulfill()
                        }}
                }
            }
        }
        projector.host = "localhost"
        
        wait(for: [powerAsserted], timeout: 2)
        mocProjector?.cancel()
    }

    func test3MuteAsserted(){
        mocProjector?.parameters.allowLocalEndpointReuse = true
        //Testing WD catches connection issues after Handshake
        let muteAsserted = XCTestExpectation(description: "Mute State Asserted")
        muteAsserted.expectedFulfillmentCount = 1
        
        mocProjector?.newConnectionHandler = handleHandShake
        self.mocProjector?.start(queue: self.q)
        
        let projector = EpsonProjector()
        projector.mute = true
        
        mocProjector?.newConnectionHandler = handleHandShake
        nextHandler = { connection in
            connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                guard let data = data else { return }
                if String(bytes: data, encoding: .ascii) ==  "MUTE ON\r\n"{
                    connection.send(content: "MUTE=OFF\r:".data(using: .ascii), completion: .contentProcessed({_ in}))
                    connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                        guard let data = data else { return }
                        if String(bytes: data, encoding: .ascii) ==  "MUTE ON\r\n"{
                            muteAsserted.fulfill()
                        }}
                }
            }
        }
        projector.host = "localhost"
        
        wait(for: [muteAsserted], timeout: 2)
        mocProjector?.cancel()
    }

    func test4SourceAsserted(){
        mocProjector?.parameters.allowLocalEndpointReuse = true
        //Testing WD catches connection issues after Handshake
        let sourceAsserted = XCTestExpectation(description: "Source State Asserted")
        sourceAsserted.expectedFulfillmentCount = 1
        
        mocProjector?.newConnectionHandler = handleHandShake
        self.mocProjector?.start(queue: self.q)
        
        let projector = EpsonProjector()
        projector.source = .HDMI
        
        mocProjector?.newConnectionHandler = handleHandShake
        nextHandler = { connection in
            connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                guard let data = data else { return }
                if String(bytes: data, encoding: .ascii) ==  "SOURCE 30\r\n"{
                    connection.send(content: "SOURCE=80\r:".data(using: .ascii), completion: .contentProcessed({_ in}))
                    connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                        guard let data = data else { return }
                        if String(bytes: data, encoding: .ascii) ==  "SOURCE 30\r\n"{
                            sourceAsserted.fulfill()
                        }}
                }
            }
        }
        projector.host = "localhost"
        
        wait(for: [sourceAsserted], timeout: 2)
        mocProjector?.cancel()
    }

}
