import XCTest
@testable import EpsonProjectorController

final class RealProjectorTests: XCTestCase {
    let projector = EpsonProjector()
    override func setUp() {
        projector.host = "EBE0441B" //EBE0441B is currently in SW106
    }
    
    func testSpilunking(){
        let expectation = XCTestExpectation(description: "Handshake")
        let handShakeCompleteSub = projector.$connectionStatus.sink{ status in
            if status == .Connected {
//                self.projector.power = true
            }
        }
        
        wait(for: [expectation], timeout: 2000)
        handShakeCompleteSub.cancel()
        
    }
    func testHandshake() {
        let expectation = XCTestExpectation(description: "Handshake")
        
        let handShakeCompleteSub = projector.$connectionStatus.sink{ status in
            if status == .Connected {expectation.fulfill()}
        }
        wait(for: [expectation], timeout: 2)
        handShakeCompleteSub.cancel()
    }
}
