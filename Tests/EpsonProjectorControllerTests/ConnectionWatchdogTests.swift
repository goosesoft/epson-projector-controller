import XCTest
import Network
import Combine
@testable import EpsonProjectorController

final class ConnectionWatchdogTests: XCTestCase {
    let q = DispatchQueue(label: "MocProjector")
    let mocProjector = try? NWListener(using: .tcp, on: 3629)

    func handleHandShake(connection:NWConnection){
        connection.start(queue: self.q)
        connection.receive(minimumIncompleteLength: 16, maximumLength: 16){data, _, _, _ in
            guard let data = data else { return }
            if data == handshakeInit {
                connection.send(content: handshakeResponce, completion: .contentProcessed({_ in}))
            }
            self.nextHandler?(connection)
        }
    }
    var nextHandler:((NWConnection) -> Void)?
    
    func test1ConnectionRefusedThenWD(){
        mocProjector?.parameters.allowLocalEndpointReuse = true //This is needed to be able to run the tests fast.
        //Testing that ConnectionStatus is set to ConnectionRefused when the supposed projector refuses a connection. I don't think there is a setting on the projector to be online but not accept controller connections, but this would help in that case. This currently will be most useful when there are big network changes and DNS has not caught up (rare).
        //Testing that the watch dog tries connecting again after a refused connection. This is useful when the network staitens out or the projector starts accapting connections.
        let connectionRefused = XCTestExpectation(description: "ConnectionRefused")
        connectionRefused.expectedFulfillmentCount = 1
        let connectionAfterWD = XCTestExpectation(description: "ConnectionAfterWD")
        connectionAfterWD.expectedFulfillmentCount = 1
        mocProjector?.newConnectionHandler = { connection in
            connection.start(queue: self.q)
        }

        let projector = EpsonProjector()
        let sub = projector.$connectionStatus.sink{status in
            if status == .ConnectionRefused {
                connectionRefused.fulfill()
                self.mocProjector?.start(queue: self.q)
            }
            if status == .WaitingOnHandShake {
                connectionAfterWD.fulfill()
            }
        }
        projector.host = "localhost"

        wait(for: [connectionRefused, connectionAfterWD], timeout: 95)
        sub.cancel()
        mocProjector?.cancel()
    }
    
    func test2NoHandShakeResponceThenWD(){
        //Testing that WD restarts connection if there is no responce to handshake.
        let waitOnHandshake = XCTestExpectation(description: "Waiting on Handshake")
        waitOnHandshake.expectedFulfillmentCount = 2
        mocProjector?.newConnectionHandler = { connection in
            connection.start(queue: self.q)
        }
        mocProjector?.start(queue: q)
        
        let projector = EpsonProjector()
        let sub = projector.$connectionStatus.sink{status in
            if status == .WaitingOnHandShake {
                waitOnHandshake.fulfill()
            }
        }
        projector.host = "localhost"

        wait(for: [waitOnHandshake], timeout: 91)
        sub.cancel()
        mocProjector?.cancel()
    }
    
    func test3illFormedHandshake() {
        //Testing that Handshake fails when "projector" response wrongly.
        let waitOnHandshake = XCTestExpectation(description: "Waiting on Handshake")
        waitOnHandshake.expectedFulfillmentCount = 1
        mocProjector?.newConnectionHandler = { connection in
            connection.start(queue: self.q)
            connection.receive(minimumIncompleteLength: 16, maximumLength: 16){data, _, _, _ in
                if data == handshakeInit {
                    connection.send(content: "I'm Not a Projector".data(using: .ascii), completion: .contentProcessed({_ in}))
                }
            }
        }
        mocProjector?.start(queue: q)
        
        let projector = EpsonProjector()
        let sub = projector.$connectionStatus.sink{status in
            if status == .HandShakeFailed {
                waitOnHandshake.fulfill()
            }
        }
        projector.host = "localhost"
        
        wait(for: [waitOnHandshake], timeout: 1)
        sub.cancel()
        mocProjector?.cancel()
    }
    
    func test4GoodHandShakeThenWD() {
        //Testing WD catches connection issues after Handshake
        let handShakeSuccessfull = XCTestExpectation(description: "Handshake Successful")
        handShakeSuccessfull.expectedFulfillmentCount = 1
        let connectionAfterWD = XCTestExpectation(description: "ConnectionAfterWD")
        connectionAfterWD.expectedFulfillmentCount = 2
        
        mocProjector?.newConnectionHandler = handleHandShake
        self.mocProjector?.start(queue: self.q)
        
        let projector = EpsonProjector()
        projector.power = true
        let sub = projector.$connectionStatus.sink{status in
            if status == .Connected {
                handShakeSuccessfull.fulfill()
            }
            if status == .WaitingOnHandShake {
                connectionAfterWD.fulfill()
            }
        }
        projector.host = "localhost"
        
        wait(for: [handShakeSuccessfull, connectionAfterWD], timeout: 95)
        sub.cancel()
        mocProjector?.cancel()
    }
    
    func test5WDCatchesQuietProjectorAfterCommand(){
        var sub:AnyCancellable?
        let roundTrip = XCTestExpectation(description: "Round Trip Complete")
        roundTrip.expectedFulfillmentCount = 1
        let wdAlerted = XCTestExpectation(description: "Watchdog Alerted")
        roundTrip.expectedFulfillmentCount = 1

        let projector = EpsonProjector()
        projector.power = true

        mocProjector?.newConnectionHandler = handleHandShake
        nextHandler = { connection in
            connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                guard let data = data else { return }
                if String(bytes: data, encoding: .ascii) ==  "PWR ON\r\n"{
                    connection.send(content: ":".data(using: .ascii), completion: .contentProcessed({_ in}))
                    connection.receive(minimumIncompleteLength: 1, maximumLength: 50){data, _, _, _ in
                        guard let data = data else { return }
                        if String(bytes: data, encoding: .ascii) ==  "PWR?\r\n"{
                            roundTrip.fulfill()
                            sub = projector.$connectionStatus.sink{ status in
                                if status == .Disconnected {
                                    wdAlerted.fulfill()
                                }
                            }
                        }
                    }

                }
            }
        }
        mocProjector?.start(queue: q)
        
        projector.host = "localhost"

        wait(for: [roundTrip, wdAlerted], timeout: 95)
        sub?.cancel()
        mocProjector?.cancel()
    }
}
