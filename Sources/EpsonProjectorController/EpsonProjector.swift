//  EpsonProjector.swift
//  Created by Lucas Goossen on 7/11/19.
import Foundation
import Combine
import Network
import OSLog
public class EpsonProjector {
    let logger = Logger(subsystem: "goosesoft.EpsonProjectorController", category: "EpsonProjector")
    //MARK: - Connection
    var connection:NWConnection?
    let q = DispatchQueue(label: "EpsonProjectorController")
    @Published var lastActivity = "Init"
    var watchDog:AnyCancellable?
    
    func setupNewConnection(){
        connection?.forceCancel()
        connection = NWConnection(host: .init(host), port: 3629, using: .tcp)
        connection?.stateUpdateHandler = connectionStateUpdateHandler
        connection?.receive(minimumIncompleteLength: 16, maximumLength: 16, completion: handShake)
        lastActivity = "Setup New Connection"
        connection?.start(queue: q)
    }
    
    func connectionStateUpdateHandler(state:NWConnection.State) {
        switch state {
        case .setup:
            logger.log("\(#function) case .setup")
            connectionStatus = .Unknown
        case .waiting(let error):
            logger.log("\(#function) case .waiting error:\(error.debugDescription, privacy: .public)")
            switch error.debugDescription    {
            case "POSIXErrorCode: Operation timed out":
                //With my current understanding, this happens when DNS resolves to an IP but nothing is responding on that IP. This would only happen if the destination has recently gone offline.
                connectionStatus = .ProjectorRecentlyUnplugged
            case "-65554: NoSuchRecord":
                //There is no sense in hurrying this along. If the controller's network is down, the connection will try again as soon as network comes back up. If the projector's network is down the WatchDog will trigger a retry every 90 seconds.
                connectionStatus = .BadHostName
            case "POSIXErrorCode: Connection refused":
                connectionStatus = .ConnectionRefused
            default:
                connectionStatus = .Unknown
            }
        case .preparing:
            connectionStatus = .Connecting
        case .ready:
            connectionStatus = .WaitingOnHandShake
            connection?.send(content:handshakeInit, completion: .contentProcessed({ (error) in
            }))
        case .failed(let error):
            logger.log("\(#function) case .failed error:\(error.debugDescription, privacy: .public)")
            connectionStatus = .Unknown
        case .cancelled:
            logger.log("\(#function) case .cancelled")
            //I think this will happen if the projector closes the conection. I think the projector will only do that if there is no comunication after a while. With the current implementation this should never heppen, so just let WatchDog trigger a reconnect.
            connectionStatus = .Unknown
        @unknown default:
            logger.log("\(#function) @unknown default")
            connectionStatus = .Unknown
        //Don't do anything, let the watchDog restart the connection if this is an error or let the connection continue if just a status update on a good connection.
        }
    }
    
    func handShake(data:Data?, context:NWConnection.ContentContext?, complete:Bool, error:NWError?) {
        if let error = error {
            logger.error("Handshaking Produced Error: \(error.debugDescription, privacy: .public)")
            connectionStatus = .HandShakeFailed
        }
        if data == handshakeResponce{
            connectionStatus = .Connected
            pollIndex = 0 //We want an instant power state reading
            lastActivity = "Successful Handshake"
            projectorReady = true
        }else {
            var responce = "empty"
            if let data = data {
                responce = String(data: data, encoding: .ascii) ?? "failed to decode"
            }
            logger.error("Handshaking Failed with ill formed responce: \(responce, privacy: .public)")
            connectionStatus = .HandShakeFailed
        }
    }
    
    //MARK: - Commands and Polling
    var pollIndex = 0
    var pollCommands = ["PWR?\r\n", "SOURCE?\r\n", "MUTE?\r\n"]
    var userCommmand:String?
    var projectorReady = false
    {
        didSet{
            if self.projectorReady {
                if let userCommand = userCommmand {
                    userCommmand = nil
                    projectorReady = false
                    logger.debug("Sending Q'ed Command:\(userCommand, privacy: .public)")
                    connection?.receive(minimumIncompleteLength: 1, maximumLength: 50, completion: processProjctorResponse)// Possible responces ":" or "ERR\r:"
                    connection?.send(content: userCommand.data(using: .ascii), completion: .contentProcessed({_ in}))
                    lastActivity = "User Command Sent"
                }else{
                    var pollInterval = DispatchTime.now() + 1
                    if !power {
                        pollInterval = DispatchTime.now() + 60
                        pollIndex = 0
                    }
                    q.asyncAfter(deadline: pollInterval){
                        guard self.projectorReady else { return }// If a userCommand just fired and colen isnt back just skip poll
                        self.projectorReady = false
                        self.logger.debug("Sending Poll:\(self.pollCommands[self.pollIndex], privacy: .public)")
                        
                        self.connection?.receive(minimumIncompleteLength: 1, maximumLength: 50, completion: self.processProjctorResponse)
                        self.connection?.send(content:
                                                self.pollCommands[
                                                    //If power off only poll power
                                                    self.power ? self.pollIndex : 0
                                                ].data(using: .ascii), completion: .contentProcessed({_ in}))
                        self.pollIndex = self.pollIndex < self.pollCommands.count - 1 ? self.pollIndex + 1 : 0
                        self.lastActivity = "Poll Sent"
                    }
                }
            }
        }
    }
    
    func send(_ command: String) {
        if projectorReady {
            logger.debug("Sending Command:\(command, privacy: .public)")
            projectorReady = false
            connection?.receive(minimumIncompleteLength: 1, maximumLength: 50, completion: processProjctorResponse)
            connection?.send(content: command.data(using: .ascii), completion: .contentProcessed({_ in}))
        }else{
            logger.debug("Q'ing Command:\(command, privacy: .public)")
            userCommmand = command
        }
    }
    
    //MARK: - Response Processing
    func processProjctorResponse(data:Data?, context:NWConnection.ContentContext?, complete:Bool, error:NWError?){
        guard let data = data, let responce = String(bytes: data, encoding: .ascii) else {return}
        logger.debug("Raw responce: \(responce)")
        projectorReady = responce.contains(":")
        let splitReponce = responce.split(separator: "=")
        let value = splitReponce.last?.split(separator: "\r").first
        switch splitReponce.first{
        case "PWR":
            switch value {
            case "0", "00":
                //some actually send "0" instead of the documented "00"
                powerStatus = .StandbyNetworkOff
                if power { power = true }
            case "01":
                powerStatus = .LampOn
                if !power { power = false }
            case "02":
                powerStatus = .Warmup
            case "03":
                powerStatus = .Cooldown
            case "04":
                powerStatus = .StandbyNetworkOn
                if power { power = true }
            case "05":
                powerStatus = .StanbyAbnormality
                if power { power = true }
            case "09":
                powerStatus = .StanbyAV
                if power { power = true }
            default:
                logger.log("Unimplemented Power Responce: \(responce , privacy: .public)")
            }
        case "MUTE":
            switch value {
            case "ON":
                if !mute { mute = false }
            case "OFF":
                if mute { mute = true }
            default:
                logger.log("Unimplemented Mute Responce: \(responce , privacy: .public)")
            }
        case "SOURCE":
            if let setSource = source{
                guard let responceSource = Source(rawValue: String(value ?? "empty")) else {
                    logger.log("Unimplemented Sourse Responce: \(responce , privacy: .public)")
                    source = setSource
                    return
                }
                if responceSource != source { source = setSource }
            }
        case "LAMP":
            guard  let hoursString = value  else { return }
            guard let hours = Int(hoursString) else { return }
            hours != lampHours ? lampHours = hours : nil
        case "ERR\r:":
            logger.debug("Projecter Says Command N/A")
        case ":":
            logger.debug("Lonesome Colon:\(responce, privacy: .public)")
        default:
            logger.log("Unimplemented Responce: \(responce, privacy: .public)")
        }
    }
    
    
    
    //MARK: - Public Interface
    @Published public var connectionStatus = NetorkStatus.Disconnected
    @Published public private(set) var lampHours = -1
    @Published public private(set) var powerStatus = PowerState.Unknown
    
    public init() {}
    public var host:String = "Not Set" {
        didSet{
            watchDog = $lastActivity.debounce(for: .seconds(90), scheduler: RunLoop.current).sink{ [weak self] activity in
                if let self = self {
                    self.logger.error("WatchDog Alerted, LastActivity: \(activity, privacy: .public), CurrentConnectionStatus: \(self.connectionStatus.rawValue, privacy: .public)")
                    self.connectionStatus = .Disconnected
                    self.setupNewConnection()
                }
            }
            setupNewConnection()
        }
    }
    
    public var power: Bool = false {
        didSet{
            send("PWR \(power ? "ON" : "OFF")\r\n")
            if !power { send("LAMP?\r\n") }
        }
    }
    public var mute: Bool = false {
        didSet{
            send("MUTE \(mute ? "ON" : "OFF")\r\n")
        }
    }
    
    public var source:Source? {
        didSet{
            if let source = source{
                send("SOURCE \(source.rawValue)\r\n")
            }
        }
    }
}
