//  Constants.swift
//  Created by Lucas Goossen on 7/12/19.
extension EpsonProjector{
    public enum NetorkStatus:String {
        case Disconnected
        case Connected
        case Connecting
        case WaitingOnHandShake
        case HandShakeFailed
        case ProjectorRecentlyUnplugged
        case ConnectionRefused
        case BadHostName
        case Unknown
    }
    
    public enum Source:String {
        case HDMI = "30"
        case HdBaseT = "80"
        case VGA = "14"
    }
    
    public enum PowerState {
        case Unknown
        case StandbyNetworkOff
        case LampOn
        case Warmup
        case Cooldown
        case StandbyNetworkOn
        case StanbyAbnormality
        case StanbyAV
    }
}
let handshakeInit = "ESC/VP.net\u{10}\u{03}\0\0\0\0".data(using: .ascii)
let handshakeResponce = "ESC/VP.net\u{10}\u{03}\0\0 \0".data(using: .ascii)
